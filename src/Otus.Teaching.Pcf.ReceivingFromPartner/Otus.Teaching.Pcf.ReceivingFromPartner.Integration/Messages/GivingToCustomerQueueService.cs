﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Messages
{
    public class GivingToCustomerQueueService : IMqService<PromoCode>
    {
        private readonly RabbitMqService _rabbitMqQueueService;

        public GivingToCustomerQueueService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);

        }
        public void SendMessage(PromoCode obj)
        {
            var element = new GivePromoCodeToCustomerDto
            {
                BeginDate = obj.BeginDate.ToString(),
                EndDate = obj.EndDate.ToString(),
                PartnerId = obj.PartnerId,
                PartnerManagerId = obj.PartnerManagerId,
                PreferenceId = obj.PreferenceId,
                PromoCode = obj.Code,
                ServiceInfo = obj.ServiceInfo,
            };
            _rabbitMqQueueService.SendMessage(element, "GivingToCustomerQueue");
        }
    }
}
