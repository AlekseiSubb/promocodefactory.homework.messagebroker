﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Messages
{
    public class AdministrationQueueService : IMqService<Guid>
    {
        private readonly RabbitMqService _rabbitMqQueueService;

        public AdministrationQueueService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);

        }
        public void SendMessage(Guid obj)
        {
            _rabbitMqQueueService.SendMessage(obj, "AdministrationQueue");
        }
    }
}
