﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Messages
{
    public interface IMqService<T>
    {
        void SendMessage(T obj);
    }
}
