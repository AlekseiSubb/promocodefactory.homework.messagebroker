﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Core.RabbitMQConsumer;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class AdministrationQueueConsumerService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Consumer _consumer;

        public AdministrationQueueConsumerService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _consumer = new Consumer(
                    configuration["RabbitMQ:Host"], 
                    configuration["RabbitMQ:User"], 
                    configuration["RabbitMQ:Password"], 
                    "AdministrationQueue");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _consumer.Dispose();
                return Task.CompletedTask;
            }
            _consumer.ReceiveMessage(HandleMessage);

            return Task.CompletedTask;
        }

        private async void HandleMessage(string message)
        {
            using var scope = _serviceProvider.CreateScope();

            if (!Guid.TryParse(message.Replace("\"", ""), out var employeeId))
                return;
            var repository = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();
            var employee = await repository.GetByIdAsync(employeeId);
            if (employee == null)
                return;
            employee.AppliedPromocodesCount++;
            await repository.UpdateAsync(employee);
        }
    }
}
