﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace Otus.Teaching.Pcf.Core.RabbitMQConsumer
{
    public class Consumer
    {
        private string _queue;

        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public Consumer(string HostName, string UserName, string Password, string Queue)
        {
            _queue = Queue;

            _connectionFactory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password
            };
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: _queue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        public void Dispose()
        {
            _channel.Dispose();
            _connection.Dispose();
        }

        public void ReceiveMessage(Action<string> handleMessage)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, eArgs) =>
            {
                var body = eArgs.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                await Task.Run(() => handleMessage(message));
            };
            _channel.BasicConsume(queue: _queue, autoAck: true, consumer: consumer);
        }
    }
}