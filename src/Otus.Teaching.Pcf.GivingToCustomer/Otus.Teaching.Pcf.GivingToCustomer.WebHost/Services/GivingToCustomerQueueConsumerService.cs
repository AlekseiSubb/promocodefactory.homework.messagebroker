﻿using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Core.RabbitMQConsumer;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class GivingToCustomerQueueConsumerService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Consumer _consumer;

        public GivingToCustomerQueueConsumerService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _serviceProvider = serviceProvider;
            _consumer = new Consumer(
                    configuration["RabbitMQ:Host"],
                    configuration["RabbitMQ:User"],
                    configuration["RabbitMQ:Password"],
                    "GivingToCustomerQueue");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _consumer.Dispose();
                return Task.CompletedTask;
            }
            _consumer.ReceiveMessage(HandleMessage);

            return Task.CompletedTask;
        }

        private async void HandleMessage(string message)
        {
            var givePromoCodeRequest = JsonConvert.DeserializeObject<GivePromoCodeRequest>(message);

            using var scope = _serviceProvider.CreateScope();
            var preferencesRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
            var customersRepository = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();
            var promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>();

            var preference = await preferencesRepository.GetByIdAsync(givePromoCodeRequest.PreferenceId);

            if (preference == null)
            {
                return;
            }

            var customers = customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id)).Result;

            PromoCode promoCode = PromoCodeMapper.MapFromModel(givePromoCodeRequest, preference, customers);

            await promoCodesRepository.AddAsync(promoCode);
        }
    }
}